# Foxden Plays Pokemon
This is a ReadyTalk Pi Day project inspired by
[Twitch Plays Pokemon](https://en.wikipedia.org/wiki/Twitch_Plays_Pok%C3%A9mon).
The intention is to have a fully automated Raspberry Pi which, upon boot, will

1. Start the VisualBoyAdvance emulator with Pokemon running
2. Launch a [Foxden](https://my.foxden.io/) conference and screenshare the emulator
3. Interpret commands in Foxden chat and execute them in the emulator.

## How to Play
The host computer should start a Foxden conference and screenshare the emulator.
To play, join this conference from another computer. You can control the game
via the chatroom with the following commands:

	up
	down
	left
	right
	a
	b
	start
	select

You can also append any number, 1-9, to a command to have it repeated that many
times (e.g. `up3` to go up 3 squares when walking around).

Multiple commands can be chained by separating them with a space. For example,
to pace back and forth a few times...

	right5 left5 right5 left5

## Components
### Chrome Extension
Foxden runs in Google Chrome (or Chromium). An extension is required to monitor
the chatroom and forward commands to the locally running server. It can also
assist with initiating the conference and screenshare.

### Local Server
A simple server (written in PHP, of course) will run on the local machine and
listen for POST requests with chat messages. It will parse them into commands
and then execute the commands in VisualBoyAdvance using `xdotool`.

The command format is as such:

    xdotool search --sync --name VisualBoyAdvance key --delay 500 Right+Right+Up+z

### Control Server
A simple API is exposed for health checks and demo requests on the
[demo page](https://jeffpi.com/foxden-plays-pokemon). This code is deployed
on Digital Ocean &mdash; you can see that code in `/control-server`. It's very
messy at the moment but a rewrite using [Lumen](https://lumen.laravel.com/) is
almost complete as of this commit.

## Installation
Set up an `nginx` server with `php5-fpm` and point the `nginx` root directory
to `/server` in this repository. The Chrome extension will hit
`https://localhost/` with its requests.

You'll need to have `xdotool` installed and run the following to grant the
nginx process access to window manipulation. **You have to run this on every reboot.**

    xhost + local:www-data
    xhost +localhost

Chrome will refuse to make requests over unencrypted HTTP. You need to generate
a self-signed certificate for `nginx` and tell Chrome to trust it. Here are some
nice guides:

* [Create a self-signed certificate for nginx](https://www.digitalocean.com/community/tutorials/how-to-create-an-ssl-certificate-on-nginx-for-ubuntu-14-04) - make sure you use `localhost` for the "common name"
* [Tell Chrome to trust this certificate](http://stackoverflow.com/a/15076602) - follow the instructions under the "edit"
