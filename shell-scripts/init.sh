#!/bin/bash

baseDirectory=~/Documents/foxden-plays-pokemon
pokemonZipPath=$baseDirectory/Pokemon.zip
pokemonRomPath=$baseDirectory/Pokemon.gbc
autostartPath=~/.config/autostart/foxden-plays-pokemon.desktop
crontabPath=/etc/cron.d/foxdenpokemon

# update codebase
pushd $baseDirectory
git pull origin master
popd

# ensure we have an nginx certificate (will require user input if not)
if [ ! -d /etc/nginx/ssl ]
	then
		sudo mkdir /etc/nginx/ssl
fi

if [ ! -f /etc/nginx/ssl/nginx.crt ]
	then
		sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/nginx/ssl/nginx.key -out /etc/nginx/ssl/nginx.crt
fi

# ensure we have a Pokemon ROM
if [ ! -f $pokemonRomPath ]
	then
		wget -O $pokemonZipPath "https://www.loveroms.com/r/Gameboy%20Color/P-T/Pokemon%20-%20Yellow%20Version%20(UE)%20[C][!].zip"
		unzip $pokemonZipPath
		mv $baseDirectory/Pokemon*.gbc $pokemonRomPath
		rm $pokemonZipPath
fi

# ensure we've installed the autostart script for Raspberry Pi
if [ ! -e $autostartPath ]; then
	ln -s $baseDirectory/shell-scripts/rpi-autostart-config.desktop $autostartPath
fi

# launch emulator and restore save state 1
export DISPLAY=:0
xhost + local:www-data && xhost +localhost && vba $pokemonRomPath &
sleep 5 && /usr/bin/xdotool search --sync --name VisualBoyAdvance key --delay 500 F1
/usr/bin/xdotool mousemove 0 0

# launch Chromium to our meeting
chromium-browser "https://my.foxden.io/#/meet/foxden.plays.pokemon@gmail.com" &
