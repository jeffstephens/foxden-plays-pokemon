#!/bin/bash

# make backup directory if necessary
backupDir=~/fpp-backup
if [ ! -d $backupDir ]
	then
		mkdir $backupDir
fi

cp ~/.vba/Pokemon1.sgm $backupDir/Pokemon-$(date +\%Y-\%m-\%d_\%H\:%M\:%S).sgm
