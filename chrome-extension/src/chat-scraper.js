window.checkTimeout = null;
window.demoCheckInterval = null;
window.healthCheckInterval = null;
window.messagesSentCount = 0;
window.demoRequestAge = 10000000;

TEN_MINUTES_IN_SECONDS = 60 * 10;

function _sendChatToServer(chatMessage) {
	var payload = {
		'chat_message' : chatMessage
	};

	console.log("*** payload: ", payload);

	jQuery.ajax({
		type: "POST",
		url: "https://localhost/",
		data: payload,
		success: function(response) { console.log("*** successfully submitted chat message", response); },
		error: function(response) { console.error("*** failed to submit chat message", response); },
		dataType: "json"
	});
}

function _resetTimeout() {
	window.checkTimeout = window.setTimeout(checkChat, 2000);
}

// inject javascript to interact with the DOM. Chrome forced
// it to come to this. Thanks, Chrome.
function _injectAndRunCode(codeToRun) {
	var script = document.createElement('script');
	script.textContent = codeToRun;
	(document.head||document.documentElement).appendChild(script);
	script.parentNode.removeChild(script);
}

function checkChat() {
	// make sure chat window is visible
	if ( ! document.getElementById("chat-history")) {
		// this probably means we're not in a conference. give up for now.
		if ( ! document.getElementById("openChat")) {
			// if a demo was requested 10 minutes ago or less
			if (window.demoRequestAge < TEN_MINUTES_IN_SECONDS) {
				// try to start or join the conference if possible
				if ($("#start-startMeeting").length > 0) {
					var startMeetingCode = '$("#start-startMeeting").click();';
					_injectAndRunCode(startMeetingCode);
				}
			}

			_resetTimeout();
			return;
		}

		// open chat pane if we're in a conference.
		var openChatCode = '$("#openChat").click();';
		_injectAndRunCode(openChatCode);

		// trigger screen share dialog if we're not already sharing
		if ( ! $("#toggleDesktopShare").find("i").hasClass("icon-screenshare-full")) {
			var shareScreenCode = '$("#toggleDesktopShare").trigger("click");';
			_injectAndRunCode(shareScreenCode);

			// select app to screen share and get this thing going (give 2 seconds to let the dialog load)
			window.setTimeout(function() {
				jQuery.ajax({
					type: "GET",
					url: "https://localhost/start-screen-share.php",
					data: {},
					success: function(response) { console.log("*** successfully started screen share", response); },
					error: function(response) { console.error("*** failed to start screen share", response); },
					dataType: "json"
				});
			}, 2000);
		}
	}

	// get all current chat messages
	var chatContainer = document.getElementById("chat-history");
	var chatMessages = chatContainer.children;

	// send the content of each chat message to the local server
	for (; window.messagesSentCount < chatMessages.length; window.messagesSentCount++) {
		var chatContent = jQuery(chatMessages.item(window.messagesSentCount)).find('p[name="chat-message-body"]').text();
		console.log("*** processing chat message: ", chatContent);

		_sendChatToServer(chatContent);
	}

	// queue up the next check
	_resetTimeout();
}

function checkDemo() {
	jQuery.ajax({
		type: "POST",
		url: "https://jeffpi.com/foxden-plays-pokemon/api/checkdemo.php",
		success: function(response) { window.demoRequestAge = response.secondsSinceDemoRequest },
		dataType: "json"
	});
}

function healthCheckin() {
	jQuery.ajax({
		type: "GET",
		url: "https://jeffpi.com/foxden-plays-pokemon/api/healthcheckin.php",
		dataType: "json",
		data: {"url" : window.location.href}
	});
}

function init() {
	// kick off the first run
	checkChat();

	// initialize demo checks every 5 seconds
	window.demoCheckInterval = window.setInterval(checkDemo, 5000);

	// initialize health checks every 10 minutes
	healthCheckin();
	window.healthCheckInterval = window.setInterval(healthCheckin, TEN_MINUTES_IN_SECONDS * 1000);
}

init();
