<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <meta name="description" content="Blog posts and live demos from Jeff's Raspberry Pi projects."/>
		<title>Jeff's Raspberry Pi Page | jeffpi.com</title>
		<link href="/bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet">
	</head>
	<body>
		<?php include('analytics.php'); ?>
		<div class="container">
			<ol class="breadcrumb">
			  <li><a href="/">Home</a></li>
			</ol>

			<div class="page-header">
				<h1>
					Jeff's Raspberry Pi Page
				</h1>
			</div>
			<p class="lead">
				Eventually I'll have some blog posts and projects listed here, but for now, I've only finished one Pi
				project.
			</p>

			<br>

      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Foxden Plays Pokemon</h3>
        </div>
        <div class="panel-body">
          <p>
            Inspired by the brilliant
            <a href="https://en.wikipedia.org/wiki/Twitch_Plays_Pok%C3%A9mon" target="_blank">Twitch Plays
            Pokemon</a>, this is a project I made for a work hackathon at
            <a href="https://www.readytalk.com/about/careers-readytalk" target="_blank">ReadyTalk</a>.
          </p>
          <p>
            It runs an emulated Pokemon Yellow game and broadcasts it via the Foxden video conferencing platform,
            which is the ReadyTalk product I work on. It accepts commands via the chat feature.
          </p>
          <p>
            Read more:
          </p>
          <p>
            <a href="/foxden-plays-pokemon/" class="btn btn-primary">Project Page: Foxden Plays Pokemon &raquo;</a>
          </p>
        </div>
      </div>

		</div>
	</body>
</html>