<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="This live demo runs an emulated Pokemon Yellow game and broadcasts it via the Foxden video conferencing platform, powered by ReadyTalk. It accepts commands via the chat feature."/>
		<title>Project Page: Foxden Plays Pokemon | jeffpi.com</title>
		<link href="/bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet">
	</head>
	<body>
    <?php include('../analytics.php'); ?>
		<div class="container">
			<ol class="breadcrumb">
			  <li><a href="/">Home</a></li>
			  <li class="active">Project Page: Foxden Plays Pokemon</li>
			</ol>

      <div class="page-header">
  			<h1>
  				Foxden Plays Pokemon
  			</h1>
      </div>
			<p class="lead">
				Play an emulated vidya game remotely via Foxden (powered by
        <a href="https://www.readytalk.com/about/careers-readytalk" target="_blank">ReadyTalk</a>).
			</p>

			<br>

      <div class="panel panel-default">
        <div class="panel-heading">
          <span class="pull-right">
            <span id="loading-health-check" class="text-muted">Checking demo...</span>
            <span id="good-health-check" class="text-success hidden">
              <span class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></span> Online
            </span>
            <span id="bad-health-check" class="text-danger hidden">
              <span class="glyphicon glyphicon-thumbs-down" aria-hidden="true"></span> Offline
            </span>
          </span>
          <h3 class="panel-title">First Things First: The Demo</h3>
        </div>
        <div class="panel-body">
          <p>
            In order to respect ReadyTalk's servers, the demo shuts itself down if it hasn't been used in while.
            No big deal, though &mdash; just poke it with the button below.
          </p>
          <p>
            You'll be taken to a Foxden conference in a new tab. Give it a few seconds to get started, then issue
            commands in the chatroom (<mark>instructions below</mark>).
          </p>
          <br>
          <p class="text-center">
            <a href="https://my.foxden.io/#/meet/foxden.plays.pokemon@gmail.com"
               class="btn btn-success btn-lg"
               disabled
               id="start-demo"
               target="_blank"><span class="glyphicon glyphicon-hand-right" aria-hidden="true"></span> Launch Demo
            </a>
          </p>
        </div>
      </div>

			<br>

      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">How to Play</h3>
        </div>
        <div class="panel-body">
          <p>
            The game is controlled via the Foxden chatroom. Click the chat icon, or press <kbd><kbd>Ctrl</kbd> + <kbd>B</kbd></kbd>
            to open chat.
          </p>
          <p>
            As this is an emulated Gameboy game, it takes Gameboy commands.
          </p>
          <ul>
            <li>
              Try <kbd><kbd>a</kbd>, <kbd>b</kbd>, <kbd>start</kbd>, <kbd>select</kbd>, <kbd>up</kbd>, <kbd>down</kbd>,
              <kbd>left</kbd>, and <kbd>right</kbd></kbd>.
            </li>
            <li>
              You can hold a button down by putting a number <code>[1-9]</code> after it: <kbd>start9</kbd>.
            </li>
            <li>
              You can also make a chain of up to 10 commands by separating them by a space: <kbd>up9 left a</kbd>.
            </li>
          </ul>
        </div>
      </div>

      <br>

      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">How it Works</h3>
        </div>
        <div class="panel-body">
          <p>
            First of all, this whole project is open-source (even this page) &mdash;
            <a href="https://gitlab.com/jeffstephens/foxden-plays-pokemon" target="_blank" class="btn btn-primary">Fork Me on Gitlab!</a>
          </p>

          <br>

          <h4>
            A Brief Presentation
          </h4>
          <p>
            Foxden Plays Pokemon was the topic of my Lightning Talk at the HTML5 Users Group meetup.
            <br><br>
            <a href="https://docs.google.com/presentation/d/1-Gy2I2uIWycl8Z4n9A8nTMKFqp0ca3pvvYE7-3O4brQ/edit?usp=sharing" target="_blank" class="btn btn-primary">View the Slides</a>
          </p>
        </div>
      </div>

		</div>
    <script type="text/javascript" src="demo-manager.js"></script>
	</body>
</html>