<?php
/**
 * Main entry point for Foxden Plays Pokemon Management API
 *
 * Handles health checkins and triggers the Raspberry Pi to start the demo
 * Foxden conference.
 */

// TODO: handle requests - can be any string in the URL.

define('SUPER_OLD_AGE', 10000000);

class FPPManagement {
	private $db;

	// =====================
	// CLASS LIFECYCLE
	// =====================

	public function __construct($config) {
		if ( ! is_array($config)) {
			self::errorResponse([
				'message' => 'No $config array found. Construction cannot continue.',
			], 500);
		}

		$this->db = new mysqli($config['db_host'], $config['db_user'], $config['db_pass'], $config['db_table']);

		if ($this->db->connect_errno) {
			FPPManagement::errorResponse([
				'message' => 'Failed to connect to mysql server.',
			], 500);
		}
	}

	public function __destruct() {
		$this->db->close();
	}

	// =====================
	// DB INSERT
	// =====================

	private function _createHealthCheck($healthy = true, $url = null) {
		if (is_null($url)) {
			$url = 'NULL';
		}

		$queryString = 'INSERT INTO healthchecks (remote_host, healthy, url) VALUES (?, ?, ?)';

		if ( ! ($queryStatement = $this->db->prepare($queryString))) {
			throw new Exception('Failed to prepare mysql query.');
		}

		if ( ! $queryStatement->bind_param('sds', $_SERVER['REMOTE_ADDR'], $healthy, $url)) {
			throw new Exception('Failed to bind parameters.');
		}

		$queryStatement->execute();
		$queryStatement->close();
	}

	private function _createDemoRequest() {
		$queryString = 'INSERT INTO demorequests (remote_host) VALUES (?)';

		if ( ! ($queryStatement = $this->db->prepare($queryString))) {
			throw new Exception('Failed to prepare mysql query.');
		}

		if ( ! $queryStatement->bind_param('s', $_SERVER['REMOTE_ADDR'])) {
			throw new Exception('Failed to bind parameters.');
		}

		$queryStatement->execute();
		$queryStatement->close();
	}

	private function _createSaveStateBackup() {
		throw new Exception('Not implemented.');
	}

	// =====================
	// DB SELECT
	// =====================

	private function _getLatestHealthyCheck() {
		$queryString = 'SELECT TIME_TO_SEC(TIMEDIFF(NOW(), timestamp)) AS latestCheckinAge FROM healthchecks ORDER BY timestamp DESC LIMIT 1';

		if ( ! $result = $this->db->query($queryString)) {
			throw new Exception('Failed to retrieve data.');
		}

		if ($result->num_rows < 1) {
			// return super old age
			return SUPER_OLD_AGE;
		}

		return intval($result->fetch_assoc()['latestCheckinAge']);
	}

	private function _getLatestDemoRequest() {
		$queryString = 'SELECT TIME_TO_SEC(TIMEDIFF(NOW(), timestamp)) AS latestRequestAge FROM demorequests ORDER BY timestamp DESC LIMIT 1';

		if ( ! $result = $this->db->query($queryString)) {
			throw new Exception('Failed to retrieve data.');
		}

		if ($result->num_rows < 1) {
			// return super old age
			return SUPER_OLD_AGE;
		}

		return intval($result->fetch_assoc()['latestRequestAge']);
	}

	private function _getLatestSaveStateBackup() {
		throw new Exception('Not implemented.');
	}

	// =====================
	// API ENDPOINTS
	// =====================

	/**
	 * Registers a health checkin so that we can track whether the Pi is up.
	 * Also returns the number of seconds since the last demo request was made,
	 * so the Pi can know whether it needs to kick off a Foxden conference.
	 * @return JSON {bool success, int secondsSinceDemoRequest}
	 */
	public function healthCheckin($url = null) {
		// TODO: implement non-healthy checks (disk full, system error, etc.)

		try {
			$this->_createHealthCheck(true, $url);
		} catch (Exception $e) {
			self::errorResponse([
				'error' => $e->getMessage(),
			], 500);
		}

		self::successResponse();
	}

	public function getHealthCheckin() {
		try {
			$latestHealthyCheck = $this->_getLatestHealthyCheck();
		} catch (Exception $e) {
			self::errorResponse([
				'error' => $e->getMessage(),
			], 500);
		}

		self::successResponse([
			'latestHealthyCheck' => $latestHealthyCheck,
		]);
	}

	public function getDemoRequest() {
		try {
			$secondsSinceDemoRequest = $this->_getLatestDemoRequest();
		} catch (Exception $e) {
			self::errorResponse([
				'error' => $e->getMessage(),
			], 500);
		}

		self::successResponse([
			'secondsSinceDemoRequest' => $secondsSinceDemoRequest,
		]);
	}

	/**
	 * Registers a request for the demo to launch.
	 * @return JSON {bool success}
	 */
	public function registerDemoRequest() {
		try {
			$this->_createDemoRequest();
		} catch (Exception $e) {
			self::errorResponse([
				'error' => $e->getMessage(),
			], 500);
		}

		self::successResponse();
	}

	/**
	 * Receives a file upload of the Gameboy emulator save state. Back this up
	 * on disk somewhere.
	 * @return JSON {bool success}
	 */
	public function backupSaveState() {
		throw new Exception('Not implemented.');
	}

	// =====================
	// JSON output
	// =====================

	/**
	 * Set the proper content header and convert an array to JSON, then output it.
	 * @param  Array $arrayToOutput The array of data to send to the client
	 * @return null
	 */
	public static function outputJson($arrayToOutput) {
		header('Content-type: application/json');
		echo json_encode($arrayToOutput);
		return;
	}

	public static function successResponse($data = []) {
		$data['success'] = true;
		self::outputJson($data);
		exit;
	}

	public static function errorResponse($data = [], $code = 400) {
		http_response_code($code);
		$data['success'] = false;
		self::outputJson($data);
		exit;
	}
}
