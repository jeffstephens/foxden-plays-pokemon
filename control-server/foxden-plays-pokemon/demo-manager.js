window.healthCheckInterval = null;

TEN_MINUTES_IN_SECONDS = 60 * 10;

document.getElementById("start-demo").onclick = function() {
	var xmlhttp=new XMLHttpRequest();
	xmlhttp.open("GET", '/foxden-plays-pokemon/api/requestdemo.php');
	xmlhttp.onreadystatechange = function() {}
	xmlhttp.send();
}

updateHealthCheck();
window.healthCheckInterval = window.setInterval(updateHealthCheck, 1000);

function updateHealthCheck() {
	var xmlhttp=new XMLHttpRequest();
	xmlhttp.open("GET", '/foxden-plays-pokemon/api/checkhealth.php');
	xmlhttp.onreadystatechange = function() {
	    if (xmlhttp.readyState == XMLHttpRequest.DONE) {
	        if(xmlhttp.status == 200) {
	        	var response = JSON.parse(xmlhttp.response);
	            if (response &&
	            	response.latestHealthyCheck &&
	            	response.latestHealthyCheck < TEN_MINUTES_IN_SECONDS) {
	            	goodHealthCheck();
	            } else {
	            	badHealthCheck();
	            }
	        } else {
	            badHealthCheck();
	        }
	    }
	}
	xmlhttp.send();
}

function goodHealthCheck() {
	console.log("good");
	document.getElementById("loading-health-check").classList.add("hidden");
	document.getElementById("bad-health-check").classList.add("hidden");
	document.getElementById("good-health-check").classList.remove("hidden");

	document.getElementById("start-demo").removeAttribute("disabled");
	document.getElementById("start-demo").classList.remove("btn-default")
	document.getElementById("start-demo").classList.add("btn-success");
}

function badHealthCheck() {
	console.log("bad");
	document.getElementById("loading-health-check").classList.add("hidden");
	document.getElementById("good-health-check").classList.add("hidden");
	document.getElementById("bad-health-check").classList.remove("hidden");

	document.getElementById("start-demo").setAttribute("disabled", "true");
	document.getElementById("start-demo").classList.remove("btn-success");
	document.getElementById("start-demo").classList.add("btn-default");
}
