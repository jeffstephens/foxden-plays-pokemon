<?php
/**
 * Server entry point for Foxden Plays Pokemon
 *
 * Listen for POST requests containing chat messages, try to parse them into
 * Gameboy commands that make sense, and then execute them via xdotool.
 */

require_once('library.php');
$FoxdenPlaysPokemon = new FPPLibrary();

if ( ! isset($_POST['chat_message']) || ! strlen($_POST['chat_message'])) {
	FPPLibrary::ajax_response('No chat message was received.', false);
	exit;
}

$raw_command_list = explode(' ', trim($_POST['chat_message']));

// no more than 10 commands allowed... come on guys
if (count($raw_command_list) > 10) {
	FPPLibrary::ajax_response("Command chains are limited to 10 at a time.", false);
	exit;
}

foreach ($raw_command_list as $command) {
	$FoxdenPlaysPokemon->parseCommand($command);
}

$FoxdenPlaysPokemon->runCommands();
FPPLibrary::ajax_response();
exit;
