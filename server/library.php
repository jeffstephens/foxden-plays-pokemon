<?php
/**
 * Support functions for Foxden Plays Pokemon server.
 */

define('BASE_CMD', 'export DISPLAY=:0; /usr/bin/xdotool search --sync --name VisualBoyAdvance key --delay 500 ');

class FPPLibrary {
	private $commands;

	public function __construct() {
		$this->commands = [];
	}

	private function enqueueCommand($cmd) {
		$cmd = escapeshellcmd($cmd);
		$cmd = BASE_CMD . $cmd;

		array_push($this->commands, $cmd);
	}

	public function parseCommand($cmd) {
		$matches = [];
		preg_match_all("/(up|down|left|right|start|select|a|b)(\d)?/i", $cmd, $matches);

		// filter out anything that's not a command
		if (count($matches[0]) === 1) {
			$gameboy_instruction = end($matches[1]);
			$n_repeat = count($matches[2]) > 0 ? end($matches[2]) : null;

			$this->enqueueCommand(self::generate_xdotool_cmd($gameboy_instruction, $n_repeat));
		}
	}

	public function runCommands() {
		foreach ($this->commands as $cmd) {
			$output = $result = null;
			exec($cmd, $output, $result);
			error_log("*** ran command $cmd with return $result, output ". print_r($output, true));
		}
	}

	public static function ajax_response($data = [], $success=true) {
		$output = [
			'success' => $success,
		];

		if (is_array($data) && count($data) > 0) {
			$output = array_merge($output, $data);
		} else if (is_string($data)) {
			$output['message'] = $data;
		}

		header('Content-type: application/json');
		echo json_encode($output);
	}

	private static function generate_xdotool_cmd($gameboy_instruction, $n_repeat) {
		if (is_null($n_repeat)) {
			$n_repeat = 0;
		} else {
			$n_repeat--; // don't double-count the base instruction
		}

		// cap repeated commands at 9 to prevent DoS
		$n_repeat = min($n_repeat, 9);

		$gameboy_instruction = self::translate_gameboy_to_emulator($gameboy_instruction);

		$command = $gameboy_instruction;
		for ($i = 0; $i < $n_repeat; $i++) {
			$command .= "+{$gameboy_instruction}";
		}

		return $command;
	}

	private static function translate_gameboy_to_emulator($command) {
		$translation_table = [
			'a' => 'z',
			'b' => 'x',
			'left' => 'Left',
			'right' => 'Right',
			'down' => 'Down',
			'up' => 'Up',
			'start' => 'Return',
			'select' => 'BackSpace',
		];

		return($translation_table[strtolower($command)]);
	}

}
